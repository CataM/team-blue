import java.util.Scanner;

public class Menu {
    Scanner scannerText = new Scanner(System.in);
    Scanner scannerNumber = new Scanner(System.in);

    
    public void startMenu() {
        ControllerAuthor controllerAuthor = new ControllerAuthor();
        controllerAuthor.addAuthors();
        ControllerBook controllerBook = new ControllerBook();
        controllerBook.addBooks();
        ControllerReviews controllerReview = new ControllerReviews();
        
        int userOption;
        String userAnswer;
        do {
            this.showMenu();
            System.out.print("Please choose an option: ");
            userOption = scannerNumber.nextInt();
            switch(userOption) {
                case 1:
                    System.out.println("List of authors: ");
                    System.out.println(" ");
                    controllerAuthor.showAuthors();
                    System.out.println(" ");
                    break;
                case 2:
                    controllerAuthor.addAuthor();
                    break;
                case 3:
                    controllerAuthor.updateAuthor();
                    break;
                case 4:
                    controllerAuthor.removeAuthors();
                    break;
                case 5:
                    System.out.println("List of books: ");
                    System.out.println(" ");
                    controllerBook.showBooks();
                    System.out.println(" ");
                    break;
                case 6:
                    controllerBook.addBook();
                    break;
                case 7:
                    controllerBook.updateBook();
                    break;
                case 8:
                    controllerBook.removeBook();
                    break;
                case 9:
                    System.out.println("List of books: ");
                    System.out.println(" ");
                    controllerBook.showBooks();
                    System.out.println("Please enter the book's number you would like to see");
                    int optionChosen = scannerNumber.nextInt();
                    Book book1 = controllerBook.bookList.get(optionChosen);
                    controllerReview.showReview(book1);
                    break;
                case 10:
                    controllerBook.showBooks();
                    System.out.println("Choose a book you want to review: ");
                    int selectedBook = scannerNumber.nextInt();
                    Book book = controllerBook.bookList.get(selectedBook);

                    System.out.println("Please enter book's score:");
                    Double bookScore = scannerNumber.nextDouble();
                    System.out.println("Please enter book's comment:");
                    String bookComment = scannerText.nextLine();
                    System.out.println("Please enter review's date:");
                    String reviewDate = scannerText.nextLine();
                    controllerReview.addReview(book, bookScore, bookComment, reviewDate);
                    break;
                case 0:
                    exit();
                    break;
                default:
                    System.out.println(" Please choose a valid option. ");
            }
            
            do {
                System.out.print("Do you want to choose another option? (Y/N): ");
                userAnswer = this.scannerText.next();
                if (!userAnswer.equalsIgnoreCase("n") && !userAnswer.equalsIgnoreCase("y")) {
                    System.out.println("Enter either Y or N!");
                }
            } while(!userAnswer.equalsIgnoreCase("n") && !userAnswer.equalsIgnoreCase("y"));
            
            if (userAnswer.equalsIgnoreCase("n")) {
                this.exit();
                return;
            }
        } while(userOption != 0 || !userAnswer.equalsIgnoreCase("n"));
        
    }
    
    public void showMenu() {
        System.out.println("----- Authors -----");
        System.out.println("1. Show authors.");
        System.out.println("2. Add author. ");
        System.out.println("3. Update author.");
        System.out.println("4. Delete author ");
        System.out.println("----- Books -----");
        System.out.println("5. Show books.");
        System.out.println("6. Add book.");
        System.out.println("7. Update book.");
        System.out.println("8. Delete book. ");
        System.out.println("----- Reviews -----");
        System.out.println("9. Show reviews.");
        System.out.println("10. Add review to existing book.");
        System.out.println("0. Exit");
    }
    
    public void exit() {
        System.out.println(" ");
        System.out.println("Thank you for choosing our library!");
        System.out.println(" ");
    }
}

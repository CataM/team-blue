


public class BookReview{
    private int id;
    private double score;
    private String comment;
    private String date;
    private Book book;



    public BookReview(Book book,double score, String comment, String date) {
       this.book = book;
        this.score = score;
        this.comment = comment;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getBookId() {
        return book.getId();
    }

    @Override
    public String toString() {
        return "BookReview{" +
                ", score=" + score +
                ", comment='" + comment + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}


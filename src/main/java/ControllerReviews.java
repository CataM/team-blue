
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ControllerReviews {
    List<BookReview> reviewList = new ArrayList<>();
    Scanner scannerText = new Scanner(System.in);
    Scanner scannerNumber = new Scanner(System.in);

    public void addReviews(Book book) {
        this.reviewList.add(new BookReview(book,10.0, "good", "07-05-2021"));
        this.reviewList.add(new BookReview(book,10.0, "very good", "09-05-2021"));
        this.reviewList.add(new BookReview(book,3.0, "bad", "08-05-2021"));
        this.reviewList.add(new BookReview(book,10.0, "good", "10-05-2021"));
        this.reviewList.add(new BookReview(book,10.0, "very good", "17-05-2021"));
    }

    public void showReview(Book book){
        List<BookReview> reviewList = this.reviewList.stream().
                filter((BookReview review)->review.getBookId()== book.getId()).collect(Collectors.toList());
        if (reviewList.isEmpty()) {
            System.out.println("Please enter reveiw first! ");
        } else {
            for(BookReview review : reviewList){
                System.out.println(review);
            }
        }

    }

    public void removeReview(Book book){
        List<BookReview> reviewList = this.reviewList.stream().
                filter((BookReview review)->review.getBookId()== book.getId()).collect(Collectors.toList());
        if (reviewList.isEmpty()) {
            System.out.println("Please enter reveiw first! ");
        } else {
            for(BookReview review : reviewList){
                System.out.println(review);
            }
            int optionChosen = scannerNumber.nextInt();
            this.reviewList.remove(optionChosen);
        }
    }

    public void addReview(Book book, double score, String comment, String date){
     BookReview review = new BookReview(book, score, comment, date);
     this.reviewList.add(review);

    }
}

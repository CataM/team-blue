
public class Book  {
    private String title;
    private String description;
    private Genre genre;
    private int id;
    
    public Book(String title, String description, Genre genre) {
        this.title = title;
        this.description = description;
        this.genre = genre;
    }
    
    public Book() {
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

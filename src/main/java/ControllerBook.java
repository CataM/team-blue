import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ControllerBook {
    List<Book> bookList = new ArrayList<>();
    Scanner scannerText = new Scanner(System.in);
    Scanner scannerNumber = new Scanner(System.in);
    
    
    public void showBooks() {
        for (int i = 0; i < bookList.size(); i++) {
            System.out.println(i + " " + bookList.get(i).toString());
        }
    }
    
    public void addBooks() {
        this.bookList.add(new Book("Pet sematery", "Horror book about pets", Genre.HORROR));
        this.bookList.add(new Book("A song of fire and ice", "Dragons and undead fighting", Genre.FANTASY));
        this.bookList.add(new Book("Eu nu strivesc corola de minuni a lumii", "Romanian poetry", Genre.THRILLER));
        this.bookList.add(new Book("No longer human", "Death is all around us", Genre.THRILLER));
        this.bookList.add(new Book("Demonii", "Russian masterpiece", Genre.FANTASY));
        //FIXME:de sters dupa conectarea cu baza de date
        for (int i = 0; i < bookList.size(); i++) {
            this.bookList.get(i).setId(i);
        }
    }
    
    public void addBook() {
        System.out.println("Please enter book title: ");
        String bookTitle = scannerText.nextLine();
        System.out.println("Please enter book description: ");
        String bookDescription = scannerText.nextLine();
        System.out.println("Please enter book genre: ");
        for (Genre genre : Genre.values()) {
            System.out.println(genre);
        }
        String bookGenreInput = scannerText.nextLine();
        Genre bookGenre = Genre.valueOf(bookGenreInput);
        
        this.bookList.add(new Book(bookTitle, bookDescription, bookGenre));
        //FIXME:de sters dupa conectarea cu baza de date
        for (int i = 0; i < bookList.size(); i++) {
            this.bookList.get(i).setId(i);
        }
    }
    
    public void updateBook() {
        showBooks();
        int optionChosen = scannerNumber.nextInt();
        System.out.println("Please enter book title: ");
        String bookTitle  = scannerText.nextLine();
        System.out.println("Please enter book description: ");
        String bookDescription  = scannerText.nextLine();
        System.out.println("Please enter book genre: ");
        for (Genre genre : Genre.values()) {
            System.out.println(genre);
        }
        String bookGenreInput  = scannerText.nextLine();
        Genre bookGenre = Genre.valueOf(bookGenreInput);
        
        Book newBook = bookList.get(optionChosen);
        newBook.setTitle(bookTitle);
        newBook.setDescription(bookDescription);
        newBook.setGenre(bookGenre);
       
        this.bookList.set(optionChosen, newBook);
    }
    
    public void removeBook() {
        showBooks();
        int optionChosen = scannerNumber.nextInt();
        this.bookList.remove(optionChosen);
    }
}
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ControllerAuthor {
    List<Author> authorList = new ArrayList<>();
    Scanner scannerText = new Scanner(System.in);
    Scanner scannerNumber = new Scanner(System.in);


    public void showAuthors() {
        for (int i = 0; i < authorList.size(); i++) {
            System.out.println(i + " " + authorList.get(i).toString());
        }
    }

    public void addAuthors() {
        this.authorList.add(new Author("King", "Stephen", "0753453453", 1951));
        this.authorList.add(new Author("Martin", "George", "0753467453", 1971));
        this.authorList.add(new Author("Blaga", "Lucian", "0753465645", 1895));
        this.authorList.add(new Author("Osamu", "Dazai", "0753455545", 1909));
        this.authorList.add(new Author("Lev", "Tolstoi", "0769754465", 1828));
    }

    public void addAuthor() {
        System.out.println("Please enter author's last name:");
        String lastName = scannerText.nextLine();
        System.out.println("Please enter author's first name:");
        String firstName = scannerText.nextLine();
        System.out.println("Please enter author's phone number name:");
        String phoneNumber = scannerText.nextLine();
        System.out.println("Please enter author's year of birth:");
        int yearOfBirth = scannerNumber.nextInt();
        this.authorList.add(new Author(lastName, firstName, phoneNumber, yearOfBirth));
    }

    public void updateAuthor() {
        showAuthors();
        int optionChosen = scannerNumber.nextInt();
        System.out.println("Please enter author's last name:");
        String lastName = scannerText.nextLine();
        System.out.println("Please enter author's first name:");
        String firstName = scannerText.nextLine();
        System.out.println("Please enter author's phone number name:");
        String phoneNumber = scannerText.nextLine();
        System.out.println("Please enter author's year of birth:");
        int yearOfBirth = scannerNumber.nextInt();

        Author newAuthor = authorList.get(optionChosen);
        newAuthor.setFirstName(firstName);
        newAuthor.setLastName(lastName);
        newAuthor.setPhoneNumber(phoneNumber);
        newAuthor.setYearOfBirth(yearOfBirth);


        this.authorList.set(optionChosen, newAuthor);
    }

    public void removeAuthors() {
        showAuthors();
        System.out.println("Please enter the number of the author you would like to delete.");
        int optionChosen = scannerNumber.nextInt();
        this.authorList.remove(optionChosen);
    }
    
    @Override
    public String toString() {
        return "ControllerAuthor{" +
                "authorList=" + authorList +
                ", scannerText=" + scannerText +
                ", scannerNumber=" + scannerNumber +
                '}';
    }
}
